<?php
/**
 * Created by PhpStorm.
 * User: n.zimmermann
 * Date: 09.03.2016
 * Time: 15:29
 */
session_start();
echo "Welcome".$_SESSION['user'];
if (isset($_POST['logout'])){
    session_start();
    session_destroy();

    header('location: /index.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="style/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <class="navbar-header">
            <div style="float: left">
            <a class="navbar-brand" href="#">Home</a>
                </div>
            <div style="float: right">
                <a class="navbar-brand" href="#">Test-Kategorie1</a>
                <a class="navbar-brand" href="#">Test-Kategorie2</a>
                <a class="navbar-brand" href="#">Test-Kategorie3</a>
                <a class="navbar-brand" href="#">Test-Kategorie4</a>
                <a class="navbar-brand" href="#" data-toggle="modal" data-target="#login-modal">Login</a>
            </div>
        <div style="clear: both">
        </div>
        </div>
    </div><!-- /.container-fluid -->
</nav>
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1>Login to Your Account</h1><br>
            <form method="post" name="logout">
                <input type="submit" name="logout" class="login loginmodal-submit" value="logout">
            </form>

            <div class="login-help">
                <a href="#">Register</a> - <a href="#">Forgot Password</a>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="style/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>