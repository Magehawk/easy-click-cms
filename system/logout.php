<?php
/**
 * Created by PhpStorm.
 * User: n.zimmermann
 * Date: 09.03.2016
 * Time: 15:29
 */
session_start();
unset($_SESSION["username"]);
unset($_SESSION["password"]);

echo 'You have cleaned session';
header('Refresh: 2; URL = login.php');