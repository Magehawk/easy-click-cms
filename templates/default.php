<?php
/**
 * Created by PhpStorm.
 * User: n.zimmermann
 * Date: 10.03.2016
 * Time: 14:36
 */

foreach($this->_['entries'] as $entry){
    ?>

    <h2><a href="?view=entry&id=<?php echo $entry['id'] ?>"><?php echo $entry['title']; ?></a></h2>
    <p><?php echo $entry['content']; ?></p>

    <?php
}
?>